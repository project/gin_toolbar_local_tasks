<?php

namespace Drupal\gin_toolbar_local_tasks;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * The Gin Toolbar Local Tasks class.
 *
 * @package Drupal\gin_toolbar_local_tasks\Toolbar
 */
class GinToolbarLocalTasks implements TrustedCallbackInterface {

  /**
   * Generate the local tasks.
   *
   * @param array $build
   *   The build array.
   *
   * @return array
   *   The build array.
   */
  public static function localTasks(array $build): array {
    $local_tasks = \Drupal::service('plugin.manager.menu.local_task')
      ->getLocalTasks(\Drupal::service('current_route_match')
        ->getRouteName(), 0);
    if (isset($local_tasks['tabs'])) {
      $local_task_default_route = '<none>';
      $local_task_default_route_params = [];
      $items = [];
      // Workaround for admin toolbar links filter.
      $dummy_original_link = FALSE;
      $first_item = reset($build['administration_menu']['#items']);
      if (isset($first_item['original_link'])) {
        $dummy_original_link = $first_item['original_link'];
      }

      // Sort by weight.
      uasort($local_tasks['tabs'], [
        'Drupal\Component\Utility\SortArray',
        'sortByWeightProperty',
      ]);
      foreach ($local_tasks['tabs'] as $key => $local_task) {
        if ($local_task['#access']->isAllowed()) {
          // Merge additional attributes for styling.
          $local_task['#link']['url'] = $local_task['#link']['url']->mergeOptions([
            'attributes' => [
              'class' => [
                'toolbar-icon',
                'toolbar-icon-local-tasks',
              ],
            ],
          ]);

          // Generate menu item with necessary properties.
          $items[] = $local_task['#link']
            + ['original_link' => $dummy_original_link]
            + [
              'attributes' => new Attribute(
                [
                  'class' => [
                    'menu-item',
                  ],
                ]),
            ];
          // Let's search for edit link if it exists.
          if (strpos($key, 'edit_form') !== FALSE) {
            $local_task_default_route = $local_task['#link']['url']->getRouteName();
            $local_task_default_route_params = $local_task['#link']['url']->getRouteParameters();
          }
        }
      }
      $new_items = [];
      if (isset($build['administration_menu']['#items']['admin_toolbar_tools.help'])) {
        // If there is admin_toolbar_tools module installed,
        // make its menu on top.
        $new_items = ['admin_toolbar_tools.help' => $build['administration_menu']['#items']['admin_toolbar_tools.help']];
        unset($build['administration_menu']['#items']['admin_toolbar_tools.help']);
      }
      $build['administration_menu']['#items'] = $new_items + [
        'local_tasks' => [
          'is_expanded' => TRUE,
          'title' => t('Local Tasks'),
          'url' => Url::fromRoute($local_task_default_route, $local_task_default_route_params, [
            'attributes' => [
              'class' => [
                'toolbar-icon',
                'toolbar-icon-local-tasks',
              ],
            ],
          ]),
          'attributes' => new Attribute([
            'class' => [
              'menu-item',
              'menu-item--expanded',
            ],
          ]),
          'below' => $items,
          'original_link' => $dummy_original_link,
        ],
      ] + $build['administration_menu']['#items'];

      // Add local tasks cacheable metadata to result build.
      $metadata = CacheableMetadata::createFromRenderArray($build['administration_menu']);
      $local_tasks['cacheability']->merge($metadata);
      $local_tasks['cacheability']->applyTo($build['administration_menu']);
    }
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['localTasks'];
  }

}
