# Gin Toolbar Local Tasks

## Contents of this file

 - Introduction
 - Requirements
 - Installation
 - Configuration

## Introduction

Helper module to bring the Local Tasks to the Drupal toolbar.

## Requirements

This module requires no additional modules.

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration.
